import 'dart:ui';

import 'package:flutter/material.dart';

// Column _buildButtonColumn(Color color, IconData icon, String label) {
//   return Column(
//     mainAxisSize: MainAxisSize.min,
//     children: [
//       Icon(
//         icon,
//         color: color,
//       ),
//       Container(
//         child: Text(
//           label,
//           style: TextStyle(
//             fontSize: 12,
//             fontWeight: FontWeight.w400,
//             color: color,
//           ),
//         ),
//       ),
//     ],
//   );
// }

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              children: [
                Container(
                    padding: const EdgeInsets.only(
                      bottom: 8,
                    ),
                    child: Text(
                      'Pannawit Juntanan',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )),
                Text('Computer Science',
                    style: TextStyle(color: Colors.grey[500]))
              ],
            ),
          ),
        ],
      ),
    );

    Widget BioSection = Container(
        padding: EdgeInsets.only(right: 20, left: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: 20,
            ),
            Text(
              'My Bio',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Padding(padding: EdgeInsets.only(top: 10)),
            Text(
              'เป็นคนสนุกสนานเข้ากับคนอื่นได้ง่าย ชอบในการเข้าสังคมและพร้อมศึกษาในระบบงาน ชอบในการเขียนโปรแกรมเข้าใจในสิ่งต่างๆได้ดี และความรับผิดชอบสูงในงานของตน มีความกระตือรีอร้นในการทำงานและศึกษาหาความรู้ใหม่ๆ เสมอ',
              softWrap: true,
            ),
          ],
        ));
    Widget skillSection = Container(
        padding: EdgeInsets.only(right: 20, left: 20, top: 40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: 20,
            ),
            Text(
              'Skills',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Padding(padding: EdgeInsets.only(top: 20)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    Container(
                        padding: EdgeInsets.only(top: 100),
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(color: Colors.white, blurRadius: 0),
                            ],
                            shape: BoxShape.circle,
                            image: new DecorationImage(
                              image: new AssetImage('./images/java.png'),
                              fit: BoxFit.cover,
                            ))),
                    Padding(padding: EdgeInsets.only(top: 10)),
                    Text(
                      'JAVA',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Container(
                        padding: EdgeInsets.only(top: 100),
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(color: Colors.white, blurRadius: 0),
                            ],
                            shape: BoxShape.circle,
                            image: new DecorationImage(
                              image: new AssetImage('./images/wordpress.png'),
                              fit: BoxFit.cover,
                            ))),
                    Padding(padding: EdgeInsets.only(top: 10)),
                    Text(
                      'WORDPRESS',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Container(
                        padding: EdgeInsets.only(top: 100),
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(color: Colors.white, blurRadius: 0),
                            ],
                            shape: BoxShape.circle,
                            image: new DecorationImage(
                              image: new AssetImage('./images/swift.png'),
                              fit: BoxFit.cover,
                            ))),
                    Padding(padding: EdgeInsets.only(top: 10)),
                    Text(
                      'SWIFTUI',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Container(
                        padding: EdgeInsets.only(top: 100),
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(color: Colors.white, blurRadius: 0),
                            ],
                            shape: BoxShape.circle,
                            image: new DecorationImage(
                              image: new AssetImage('./images/java-script.png'),
                              fit: BoxFit.cover,
                            ))),
                    Padding(padding: EdgeInsets.only(top: 10)),
                    Text(
                      'JAVA SCRIPT',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ],
            )
          ],
        ));
    Widget contectSection = Container(
        padding: EdgeInsets.only(right: 20, left: 20, top: 40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: 20,
            ),
            Text(
              'Contact',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Padding(padding: EdgeInsets.only(top: 10)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    Container(
                        padding: EdgeInsets.only(top: 100),
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(color: Colors.black, blurRadius: 0),
                            ],
                            shape: BoxShape.circle,
                            image: new DecorationImage(
                              image: new AssetImage('./images/line.png'),
                              fit: BoxFit.cover,
                            ))),
                    Padding(padding: EdgeInsets.only(top: 10)),
                    Text(
                      'line',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Container(
                        padding: EdgeInsets.only(top: 100),
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(color: Colors.black, blurRadius: 0),
                            ],
                            shape: BoxShape.circle,
                            image: new DecorationImage(
                              image: new AssetImage('./images/facebook.png'),
                              fit: BoxFit.cover,
                            ))),
                    Padding(padding: EdgeInsets.only(top: 10)),
                    Text(
                      'facebook',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Container(
                        padding: EdgeInsets.only(top: 100),
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(color: Colors.black, blurRadius: 0),
                            ],
                            shape: BoxShape.circle,
                            image: new DecorationImage(
                              image: new AssetImage('./images/instagram.png'),
                              fit: BoxFit.cover,
                            ))),
                    Padding(padding: EdgeInsets.only(top: 10)),
                    Text(
                      'instagram',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Container(
                        padding: EdgeInsets.only(top: 100),
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(color: Colors.black, blurRadius: 0),
                            ],
                            shape: BoxShape.circle,
                            image: new DecorationImage(
                              image: new AssetImage('./images/twitter.png'),
                              fit: BoxFit.cover,
                            ))),
                    Padding(padding: EdgeInsets.only(top: 10)),
                    Text(
                      'twitter',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ],
            )
          ],
        ));
    Widget imageSection = Container(
      width: 200,
      height: 200,
      color: Colors.transparent,
      child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Container(
              decoration: BoxDecoration(
                  boxShadow: [
                BoxShadow(color: Colors.black, blurRadius: 0),
              ],
                  shape: BoxShape.circle,
                  image: new DecorationImage(
                    image: new AssetImage('./images/myimage.jpg'),
                    fit: BoxFit.cover,
                  )))),
    );

    Widget educationSection = Container(
      padding: EdgeInsets.only(right: 20, left: 20, top: 40),
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Education History',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Padding(padding: EdgeInsets.only(top: 20)),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '2012 - 2017 : Satri Angthong School',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Padding(padding: EdgeInsets.only(top: 10)),
                  Text(
                    '2018 - ปัจจุบัน : Burapha University',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ],
              )
            ],
          )
        ],
      ),
    );

    return MaterialApp(
      title: 'Pannawit Juntanan',
      home: Scaffold(
          appBar: AppBar(
            title: const Text(
              'Resume',
            ),
          ),
          body: Column(children: [
            imageSection,
            titleSection,
            BioSection,
            skillSection,
            contectSection,
            educationSection
          ])),
    );
  }
}
